// A. Ascending Ten
function AscendingTen(num) {
    let result = "";
    let numMax = num + 10;
    
    if (num != undefined){
        for (let i = num; i < numMax; i++) {
            result = result + ` ${String(i)}`;
        }
    } else {
        result = "-1"
    }
    return result;
}

console.log(AscendingTen(11)); // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)); // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()); // -1



// B. Descending Ten
function DescendingTen(num) {
    let result = "";
    let numMax = num - 10;
    
    if (num != undefined){
        for (let i = num; i > numMax; i--) {
            result = result + ` ${String(i)}`;
        }
    } else {
        result = "-1"
    }
    return result;
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


// C. Conditional Ascending Descending
function ConditionalAscDesc(reference, check) {
    let result = "";
    
    if (check % 2 == 0){ // Genap
        let numMax = reference - 10;
        if (reference != undefined){
            for (let i = reference; i > numMax; i--) {
                result = result + ` ${String(i)}`;
            }
        } else {
            result = "-1"
        }
    } else if (check % 2 == 1) { // Ganjil
        let numMax = reference + 10;

        if (reference != undefined){
            for (let i = reference; i < numMax; i++) {
                result = result + ` ${String(i)}`;
            }
        } else {
            result = "-1"
        }
    } else {
        result = "-1"
    }
    return result;
}

console.log(ConditionalAscDesc(20, 8)); // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)); // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)); // -1
console.log(ConditionalAscDesc()); // -1


// D. Papan Ular Tangga
function ularTangga() {
    let result = "";
    let arr = [];

    for (let i = 100; i >= -1; i--) {
        if (arr.length >= 10){
            let strArr = `${arr.join(" ")}\n`;
            result += strArr;
            arr = [];
            
            arr.push(i);
        } else {
            arr.push(i);
        }
        
    }
    return result;
}

console.log(ularTangga());