// A. Balik String
function balikString(str) {
    let current = str;
    let word = '';

    for (let i = str.length - 1; i >= 0; i--) {
        word = word + current[i];
    }
    return word;
}

console.log("---------------------- A. Balik String ----------------------");
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// B. Palindrome
function palindrome(str) {
    var exp = /[^A-Za-z0-9]/g;
    str = str.toLowerCase().replace(exp, '');
    var strlength = str.length;
    for (var i = 0; i < strlength/2; i++) {
      if (str[i] !== str[strlength - 1 - i]) {
          return false;
      }
    }
    return true;
}

console.log("---------------------- B. Palindrome ------------------------");
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


// C. Bandingkan Angka
function bandingkan(num1, num2) {
    let result;

    if(num1 > 0 && num2 > 0){
        if (num1 > num2){
            result = num1;
        } else if (num1 == num2){
            result = -1;
        }else {
            result = num2
        }
    } else {
        if(num1 != undefined){
            result = num1;
        } else if (num2 != undefined) {
            result = num2;
        } else {
            result = -1;
        }
    }
    return result;
}

console.log("---------------------- C. Bandingkan Angka-------------------");
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18